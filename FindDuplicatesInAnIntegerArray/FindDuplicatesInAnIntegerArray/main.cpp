#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

class Engine
{
    private:
        vector<int> arrVector;
    
        void displayDuplicateVector(vector<int> duplicateVector)
        {
            int len = (int)duplicateVector.size();
            for(int i = 0 ; i < len ; i++)
            {
                cout<<duplicateVector[i]<<" ";
            }
            cout<<endl;
        }
    
    public:
        Engine(vector<int> aV)
        {
            arrVector = aV;
        }
    
        void findDuplicateElements()
        {
            vector<int> duplicatesVector;
            int         len = (int)arrVector.size();
            for(int i = 0 ; i < len ; i++)
            {
                if(arrVector[abs(arrVector[i])] < 0)
                {
                    duplicatesVector.push_back(abs(arrVector[i]));
                }
                arrVector[abs(arrVector[i])] *= -1;
            }
            displayDuplicateVector(duplicatesVector);
        }
};

int main(int argc, const char * argv[])
{
//    vector<int> arrVector = {2,4,1,2,6,1,6,3,0};
    vector<int> arrVector = {1,2,4,3,4,6,2,0,3,0,1};
    Engine      e         = Engine(arrVector);
    e.findDuplicateElements();
    return 0;
}
